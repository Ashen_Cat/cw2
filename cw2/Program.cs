﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace cw2
{
    class Program
    {
        static void Main(string[] args)
        {
            //data files
            const string Data = "..\\..\\Don.txt";
            const string Result = "..\\..\\Result.txt";
            //container
            FoodList food = new FoodList();
            //reading from file data
            Read(Data, food);
            //new container
            FoodList newFood = new FoodList();
            //reforming container by weight
            newFood.ReWeight(food, 250);
            //printing data
            Sort(newFood);
            using (StreamWriter Writer = new StreamWriter(Result))
            {
                string block  = "|----------------------|-----------------|------------|\n";
                       block += "|                 name |            type |     weight |\n";
                       block += "|----------------------|-----------------|------------|";
                //printing old food data
                Writer.WriteLine("Old food data");
                Writer.WriteLine(block);
                Print(Writer, food);
                Writer.WriteLine("|----------------------|-----------------|------------|");
                //printing new food data
                Writer.WriteLine("New food data");
                Writer.WriteLine(block);
                Print(Writer, newFood);
                Writer.WriteLine("|----------------------|-----------------|------------|");


            }
        }
        //read method
        static void Read(string Data, FoodList food)
        {
            using(StreamReader Reader = new StreamReader(Data))
            {
                string line;
                while ((line = Reader.ReadLine()) != null)
                {
                    string[] parts = line.Split(';');
                    FoodItem temporary = new FoodItem(parts[0], parts[1], int.Parse(parts[2]));
                    food.Set(temporary);
                }
            }
           
        }
        //Printing method
        static void Print(StreamWriter Writer, FoodList food)
        {
            for (int i = 0; i < food.N(); i++)
                Writer.WriteLine(food.Geti(i).ToString());
        }
        static void Sort(FoodList food)
        {
            for(int i = 0; i < food.N() -1; i++)
                for(int j = i+1; j < food.N(); j++)
                {
                    if(food.Geti(i) < food.Geti(j))
                    {
                        food.Geti(j).Swap(food.Geti(i));
                    }
                }
        }

    }
}
