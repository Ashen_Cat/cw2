﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
    class FoodItem
    {
        //variables with their get set methods
        public string Name { get; set; }
        public string Type { get; set; }
        public int Weight { get; set; }

        //constructors
        public FoodItem()
        {
            Name = null;
            Type = null;
            Weight = 0;
        }
        public FoodItem(string n, string t, int w)
        {
                Name = n;
                Type = t;
                Weight = w;
        }
        //Tostring() override
        public override string ToString()
        {
            return string.Format("| {0, -20:c} | {1, -15:c} | {2, -10:d} |", this.Name, this.Type, this.Weight);
        }
        //food swap method
        public void Swap(FoodItem other)
        {
            FoodItem tempa = new FoodItem(other.Name, other.Type, other.Weight);
            FoodItem tempb = new FoodItem(this.Name, this.Type, this.Weight);

            this.Name = tempa.Name;
            this.Type = tempa.Type;
            this.Weight = tempa.Weight;

            other.Name = tempb.Name;
            other.Type = tempb.Type;
            other.Weight = tempb.Weight;
        }
        public static bool operator >( FoodItem bigger, FoodItem smaller)
        {
            return (bigger.Type.CompareTo(smaller.Type) < 0);
        }
        public static bool operator <(FoodItem bigger, FoodItem smaller)
        {
            return (bigger.Type.CompareTo(smaller.Type) > 0);
        }
    }
}
