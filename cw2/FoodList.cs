﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
    class FoodList
    {
        List<FoodItem> food;
        //constructor
        public FoodList() { food = new List<FoodItem>(); }

        //get set methods
        public void Set(FoodItem f) { food.Add(f); }
        public FoodItem Geti(int i) { return food[i]; }
        public void Seti(FoodItem f, int i) { food[i] = f; }
        public int N() { return food.Count; }

        //average weight calculation method
        public int Average()
        {
            int average = 0;

            foreach (FoodItem item in food)
                average += item.Weight;

            average /= food.Count;

            return average;
        }
        //reforming by weight
        public void ReWeight(FoodList other, int weight)
        {
            for (int i = 0; i < other.N(); i++)
                if (other.Geti(i).Weight >= weight)
                    this.food.Add(other.Geti(i));
        }
    }
}
